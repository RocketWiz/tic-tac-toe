﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe_v1
{
    public partial class Form1 : Form
    {
        bool turnX = true;
        int c = 0;
        int XWins = 0;
        int OWins = 0;
        int ties = 0;
        bool tieTurn = true;
        public Form1()
        {
            InitializeComponent();
        }
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            PictureBox pressPic = (PictureBox)sender;
            if ((string)pressPic.Tag == "n")
            {
                if(turnX)
                {
                    pressPic.Image = Properties.Resources.X;
                    pressPic.Tag = "x";
                }
                else
                {
                    pressPic.Image = Properties.Resources.O;
                    pressPic.Tag = "o";
                }
                turnX = !turnX;
                if (turnX)
                {
                    lblTurn.Text = "X";
                }
                else
                {
                    lblTurn.Text = "O";
                }
                c++;
                if (c > 4)
                {
                    if (winCheck() == 1)
                    {
                        XWins++;
                        lblXWins.Text = Convert.ToString(XWins);
                        turnX = true;
                        lblTurn.Text = "X";
                        tieTurn = false;
                        noTagSet();
                    }
                    else if (winCheck() == 2)
                    {
                        OWins++;
                        lblOWins.Text = Convert.ToString(OWins);
                        turnX = false;
                        lblTurn.Text = "O";
                        tieTurn = true;
                        noTagSet();
                    }
                    else if (winCheck() == 0)
                    {
                        ties++;
                        lblTies.Text = Convert.ToString(ties);
                        if ((XWins > 0 || OWins > 0) && ties == 0)
                            turnX = tieTurn;
                        else if (ties == 1 && XWins == 0 && OWins == 0)
                            turnX = false;
                            noTagSet();
                    }
                }
            }
        }
        public int winCheck()
        {
            //1 for X win, 2 for O, 0 for tie
            if (pb1.Tag == pb2.Tag && pb2.Tag == pb3.Tag && (string)pb1.Tag != "n")
                return(checkPlayer((string)pb1.Tag));
            if (pb4.Tag == pb5.Tag && pb5.Tag == pb6.Tag && (string)pb4.Tag != "n")
                return (checkPlayer((string)pb4.Tag));
            if (pb7.Tag == pb8.Tag && pb8.Tag == pb9.Tag && (string)pb7.Tag != "n")
                return (checkPlayer((string)pb7.Tag));
            if (pb1.Tag == pb4.Tag && pb4.Tag == pb7.Tag && (string)pb1.Tag != "n")
                return (checkPlayer((string)pb1.Tag));
            if (pb2.Tag == pb5.Tag && pb5.Tag == pb8.Tag && (string)pb2.Tag != "n")
                return (checkPlayer((string)pb2.Tag));
            if (pb3.Tag == pb6.Tag && pb6.Tag == pb9.Tag && (string)pb3.Tag != "n")
                return (checkPlayer((string)pb3.Tag));
            if (pb1.Tag == pb2.Tag && pb2.Tag == pb3.Tag && (string)pb1.Tag != "n")
                return (checkPlayer((string)pb1.Tag));
            if (pb1.Tag == pb5.Tag && pb5.Tag == pb9.Tag && (string)pb1.Tag != "n")
                return (checkPlayer((string)pb1.Tag));
            if (pb3.Tag == pb5.Tag && pb5.Tag == pb7.Tag && (string)pb3.Tag != "n")
                return (checkPlayer((string)pb3.Tag));
            else if ((string)pb1.Tag != "n" && (string)pb2.Tag != "n" && (string)pb3.Tag != "n" && (string)pb4.Tag != "n" && (string)pb5.Tag != "n" && (string)pb6.Tag != "n" && (string)pb7.Tag != "n" && (string)pb8.Tag != "n" && (string)pb9.Tag != "n")
                return 0;
            return 300;
        }
        public int checkPlayer(string t)
        {
            if (t == "x")
                return 1;
            return 2;
        }
        private void btnNewGame_Click(object sender, EventArgs e)
        {
            nullTagSet();
            nullImageSet();
        }
        private void nullImageSet()
        {
            pb1.Image = null;
            pb2.Image = null;
            pb3.Image = null;
            pb4.Image = null;
            pb5.Image = null;
            pb6.Image = null;
            pb7.Image = null;
            pb8.Image = null;
            pb9.Image = null;
        }
        private void nullTagSet()
        {
            pb1.Tag = "n";
            pb2.Tag = "n";
            pb3.Tag = "n";
            pb4.Tag = "n";
            pb5.Tag = "n";
            pb6.Tag = "n";
            pb7.Tag = "n";
            pb8.Tag = "n";
            pb9.Tag = "n";
        }
        private void noTagSet()
        {
            pb1.Tag = "no";
            pb2.Tag = "no";
            pb3.Tag = "no";
            pb4.Tag = "no";
            pb5.Tag = "no";
            pb6.Tag = "no";
            pb7.Tag = "no";
            pb8.Tag = "no";
            pb9.Tag = "no";
        }
    }
}