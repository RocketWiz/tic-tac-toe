﻿namespace TicTacToe_v1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNewGame = new System.Windows.Forms.Button();
            this.lblXWins = new System.Windows.Forms.Label();
            this.txtXWins = new System.Windows.Forms.TextBox();
            this.txtOWins = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblOWins = new System.Windows.Forms.Label();
            this.pb2 = new System.Windows.Forms.PictureBox();
            this.pb3 = new System.Windows.Forms.PictureBox();
            this.pb6 = new System.Windows.Forms.PictureBox();
            this.pb9 = new System.Windows.Forms.PictureBox();
            this.pb4 = new System.Windows.Forms.PictureBox();
            this.pb7 = new System.Windows.Forms.PictureBox();
            this.pb8 = new System.Windows.Forms.PictureBox();
            this.pb5 = new System.Windows.Forms.PictureBox();
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTies = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTurn = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNewGame
            // 
            this.btnNewGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewGame.Location = new System.Drawing.Point(60, 463);
            this.btnNewGame.Name = "btnNewGame";
            this.btnNewGame.Size = new System.Drawing.Size(220, 61);
            this.btnNewGame.TabIndex = 10;
            this.btnNewGame.Text = "Start New Game";
            this.btnNewGame.UseVisualStyleBackColor = true;
            this.btnNewGame.Click += new System.EventHandler(this.btnNewGame_Click);
            // 
            // lblXWins
            // 
            this.lblXWins.AutoSize = true;
            this.lblXWins.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblXWins.Location = new System.Drawing.Point(104, 219);
            this.lblXWins.Name = "lblXWins";
            this.lblXWins.Size = new System.Drawing.Size(0, 25);
            this.lblXWins.TabIndex = 11;
            this.lblXWins.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtXWins
            // 
            this.txtXWins.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtXWins.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtXWins.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXWins.Location = new System.Drawing.Point(58, 166);
            this.txtXWins.Name = "txtXWins";
            this.txtXWins.Size = new System.Drawing.Size(100, 24);
            this.txtXWins.TabIndex = 12;
            this.txtXWins.Text = "X Wins";
            this.txtXWins.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtOWins
            // 
            this.txtOWins.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtOWins.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOWins.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOWins.Location = new System.Drawing.Point(180, 166);
            this.txtOWins.Name = "txtOWins";
            this.txtOWins.Size = new System.Drawing.Size(100, 24);
            this.txtOWins.TabIndex = 13;
            this.txtOWins.Text = "O Wins";
            this.txtOWins.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(612, 406);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 25);
            this.label1.TabIndex = 14;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOWins
            // 
            this.lblOWins.AutoSize = true;
            this.lblOWins.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOWins.Location = new System.Drawing.Point(230, 219);
            this.lblOWins.Name = "lblOWins";
            this.lblOWins.Size = new System.Drawing.Size(0, 25);
            this.lblOWins.TabIndex = 14;
            this.lblOWins.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pb2
            // 
            this.pb2.Location = new System.Drawing.Point(694, 97);
            this.pb2.Name = "pb2";
            this.pb2.Size = new System.Drawing.Size(165, 157);
            this.pb2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb2.TabIndex = 9;
            this.pb2.TabStop = false;
            this.pb2.Tag = "n";
            this.pb2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pb3
            // 
            this.pb3.Location = new System.Drawing.Point(978, 97);
            this.pb3.Name = "pb3";
            this.pb3.Size = new System.Drawing.Size(165, 157);
            this.pb3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb3.TabIndex = 8;
            this.pb3.TabStop = false;
            this.pb3.Tag = "n";
            this.pb3.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pb6
            // 
            this.pb6.Location = new System.Drawing.Point(978, 353);
            this.pb6.Name = "pb6";
            this.pb6.Size = new System.Drawing.Size(165, 157);
            this.pb6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb6.TabIndex = 7;
            this.pb6.TabStop = false;
            this.pb6.Tag = "n";
            this.pb6.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pb9
            // 
            this.pb9.Location = new System.Drawing.Point(978, 596);
            this.pb9.Name = "pb9";
            this.pb9.Size = new System.Drawing.Size(165, 157);
            this.pb9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb9.TabIndex = 6;
            this.pb9.TabStop = false;
            this.pb9.Tag = "n";
            this.pb9.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pb4
            // 
            this.pb4.Location = new System.Drawing.Point(407, 353);
            this.pb4.Name = "pb4";
            this.pb4.Size = new System.Drawing.Size(165, 157);
            this.pb4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb4.TabIndex = 5;
            this.pb4.TabStop = false;
            this.pb4.Tag = "n";
            this.pb4.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pb7
            // 
            this.pb7.Location = new System.Drawing.Point(407, 596);
            this.pb7.Name = "pb7";
            this.pb7.Size = new System.Drawing.Size(165, 157);
            this.pb7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb7.TabIndex = 4;
            this.pb7.TabStop = false;
            this.pb7.Tag = "n";
            this.pb7.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pb8
            // 
            this.pb8.Location = new System.Drawing.Point(694, 596);
            this.pb8.Name = "pb8";
            this.pb8.Size = new System.Drawing.Size(165, 157);
            this.pb8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb8.TabIndex = 3;
            this.pb8.TabStop = false;
            this.pb8.Tag = "n";
            this.pb8.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pb5
            // 
            this.pb5.Location = new System.Drawing.Point(694, 353);
            this.pb5.Name = "pb5";
            this.pb5.Size = new System.Drawing.Size(165, 157);
            this.pb5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb5.TabIndex = 2;
            this.pb5.TabStop = false;
            this.pb5.Tag = "n";
            this.pb5.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pb1
            // 
            this.pb1.Location = new System.Drawing.Point(407, 97);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(165, 157);
            this.pb1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb1.TabIndex = 1;
            this.pb1.TabStop = false;
            this.pb1.Tag = "n";
            this.pb1.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::TicTacToe_v1.Properties.Resources.Grid;
            this.pictureBox1.Location = new System.Drawing.Point(349, 55);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(852, 730);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::TicTacToe_v1.Properties.Resources.line;
            this.pictureBox2.Location = new System.Drawing.Point(164, 166);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(10, 124);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::TicTacToe_v1.Properties.Resources.line;
            this.pictureBox3.Location = new System.Drawing.Point(70, 193);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(201, 10);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 16;
            this.pictureBox3.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(77, 336);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 25);
            this.label2.TabIndex = 17;
            this.label2.Text = "Ties:";
            // 
            // lblTies
            // 
            this.lblTies.AutoSize = true;
            this.lblTies.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTies.Location = new System.Drawing.Point(142, 336);
            this.lblTies.Name = "lblTies";
            this.lblTies.Size = new System.Drawing.Size(0, 25);
            this.lblTies.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(8, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(324, 33);
            this.label3.TabIndex = 19;
            this.label3.Text = "X Starts, and the winner";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(52, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(231, 33);
            this.label4.TabIndex = 20;
            this.label4.Text = "starts next game";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(104, 550);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 29);
            this.label5.TabIndex = 21;
            this.label5.Text = "Now Plays:";
            // 
            // lblTurn
            // 
            this.lblTurn.AutoSize = true;
            this.lblTurn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurn.Location = new System.Drawing.Point(158, 596);
            this.lblTurn.Name = "lblTurn";
            this.lblTurn.Size = new System.Drawing.Size(30, 29);
            this.lblTurn.TabIndex = 22;
            this.lblTurn.Text = "X";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1225, 836);
            this.Controls.Add(this.lblTurn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblTies);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblOWins);
            this.Controls.Add(this.txtOWins);
            this.Controls.Add(this.txtXWins);
            this.Controls.Add(this.lblXWins);
            this.Controls.Add(this.btnNewGame);
            this.Controls.Add(this.pb2);
            this.Controls.Add(this.pb3);
            this.Controls.Add(this.pb6);
            this.Controls.Add(this.pb9);
            this.Controls.Add(this.pb4);
            this.Controls.Add(this.pb7);
            this.Controls.Add(this.pb8);
            this.Controls.Add(this.pb5);
            this.Controls.Add(this.pb1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.PictureBox pb5;
        private System.Windows.Forms.PictureBox pb8;
        private System.Windows.Forms.PictureBox pb7;
        private System.Windows.Forms.PictureBox pb4;
        private System.Windows.Forms.PictureBox pb9;
        private System.Windows.Forms.PictureBox pb6;
        private System.Windows.Forms.PictureBox pb3;
        private System.Windows.Forms.PictureBox pb2;
        private System.Windows.Forms.Button btnNewGame;
        private System.Windows.Forms.Label lblXWins;
        private System.Windows.Forms.TextBox txtXWins;
        private System.Windows.Forms.TextBox txtOWins;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblOWins;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTies;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblTurn;
    }
}

